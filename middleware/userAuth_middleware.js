const jwt = require("jsonwebtoken");
const fs = require("fs");
const path = require("path");
const private_key = fs.readFileSync(path.join(__filename, "../../config/key_private.key"));

const userAuth_middleware = {
  withAuth: function(req, res, next) {
    const token = req.body.token || req.query.token || req.headers["x-access-token"] || req.cookies.token;
    if (!token) {
      res.status(401).send("Unauthorized: No token provided");
    } else {
      jwt.verify(token, private_key, function(err, decoded) {
        if (err) {
          res.status(401).send("Unauthorized: Invalid token");
        } else {
          const { id, type } = decoded;
          req.user = { id, type };
          next();
        }
      });
    }
  },
  private_key: private_key
};

module.exports = {
  userAuth_middleware
};
