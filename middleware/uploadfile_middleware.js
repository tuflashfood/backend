const multer = require("multer");
const path = require("path");
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, path.join(__dirname + "/../public"));
  },
  filename: function(req, file, cb) {
    // cb(null, file.originalname + "." + file.mimetype.split("/")[1]);
    cb(null, file.originalname);
  }
});

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb("Please upload only images.", false);
  }
};

const upload = multer({
  limits: {
    fileSize: 4 * 1024 * 1024
  },
  storage: storage
  // fileFilter: multerFilter
});
console.log();

module.exports = { upload, multer };
