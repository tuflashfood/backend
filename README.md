# Project Tu Flash Food

* Back End  
* Language Javascript  
* Framework Express
* API Google Map Api

## Install 

### Clone 
* Clone this repo to your local machine using https://gitlab.com/tuflashfood/backend.git

### Setup

   #### 1. NodeJs
   - use the package manager npm to install node

```bash
$ npm install
```

   - command start express
 
```bash
$ npm run server
``` 

   #### 2. Google Map Api
   * **get key google map api** change key google map in file .env (variable KEY_SERVER = ``)
     * link <a href="https://www.makewebeasy.com/blog/google-map-api-key-manual/" target="_blank">`google map api key`</a>.
 

## Directory Tree
```
├── config
│   ├── database_config.js
│   ├── firebase_config.js
│   ├── keypair_singapore.pem
│   ├── key_private.key
│   └── tuff_api_db_1.5.1.sql
├── controllers
│   ├── order_controller.js
│   ├── restaurant_controller.js
│   ├── sender_controller.js
│   ├── userAuth_controller.js
│   └── user_controller.js
├── .env
├── function
│   ├── function.js
│   ├── queue.js
│   └── uploadfile.js
├── .gitignore
├── index.js
├── middleware
│   ├── uploadfile_middleware.js
│   └── userAuth_middleware.js
├── models
│   ├── google_map_model.js
│   ├── order_model.js
│   ├── restaurant_model.js
│   ├── sender_model.js
│   └── user_model.js
├── package.json
├── package-lock.json
├── README.md
└── routes
    └── router.js
```

