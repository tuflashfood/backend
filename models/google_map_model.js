const dotenv = require("dotenv");
const axios = require("axios");
require("dotenv").config();
// console.log(process.env.KEY_GOOGLE_MAP);

const google_map = {
  getNameLatLongToAddress: function (latlong) {
    return axios.get(
      `https://maps.googleapis.com/maps/api/geocode/json?&address=${latlong}&language=th&key=` +
        process.env.KEY_GOOGLE_MAP
    );
  },
};

module.exports = google_map;
