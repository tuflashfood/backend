const { pool } = require("../config/database_config");
const functions = require("../function/function");
const order_model = {
	table_name: "public.order",
	// searchResturantNear: function(req, res) {},
	confirmOrder: function (req, res) {},
	updateStatusOrder: function (order_id, order_status) {
		return new Promise(async (resolve, reject) => {
			// check key of order_status
			// for use assign to resultStaus
			const keys = Object.keys(order_status);

			let error, result;
			({ error, result } = await functions.to(
				order_model.getOrderById(order_id)
			));

			if (error) {
				reject(error);
				console.log(error + " error");
				return;
			}

			let resultStatus = result.order_status;
			console.log(resultStatus);

			resultStatus[keys[0]] = order_status[keys[0]];
			pool
				.query(
					`UPDATE ${
						order_model.table_name
					} SET order_status = '${JSON.stringify(
						resultStatus
					)}' WHERE order_id = ${order_id}`
				)
				.then((result) => {
					if (result.rowCount > 0)
						resolve(`update status ${order_status} success`);
					reject("update status not succes");
				})
				.catch((err) => reject(err));
		});
	},
	createOrder: function (
		res_id,
		user_id,
		orderStatus,
		food_order,
		billing_cost,
		destination_location,
		destination_location_name
	) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`INSERT INTO ${
						order_model.table_name
					} (res_id, buy_type_user_id, order_status, food_order, destination_location,destination_location_name
        , billing_cost , sender_type_id , order_complete , order_restaurant) VALUES (${res_id}, ${user_id}, '${JSON.stringify(
						orderStatus
					)}', '${JSON.stringify(food_order)}', point(${destination_location}),
          '${destination_location_name}',${billing_cost} , null , false , false)`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve("Insert to database sucess");
					reject("Insert to database fail");
				})
				.catch((err) => {
					reject("error " + err);
				});
		});
	},
	getOrderByIdResIdUserOrderStatus: function (res_id, user_id, orderStatus) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT tf.order_id , tf.res_id , tf.buy_type_user_id , tf.order_status , tf.food_order , tf.destination_location , tf.destination_location_name , tf.billing_cost , tf.sender_type_id , tf.order_complete , tf.order_restaurant, ts."location" , ts.res_name , ts.delivery_cost FROM ${
						order_model.table_name
					} tf INNER JOIN restaurant ts ON tf.res_id = ts.res_id WHERE tf.res_id=${res_id} AND tf.buy_type_user_id=${user_id} AND tf.order_status::jsonb ='${JSON.stringify(
						orderStatus
					)}'::jsonb LIMIT 1`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows[0]);
					reject("data not found");
				})
				.catch((err) => reject(err));
		});
	},
	getOrderById: function (order_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${order_model.table_name} WHERE order_id=${order_id} LIMIT 1`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows[0]);
					reject("data not found");
				})
				.catch((err) => reject(err));
		});
	},
	getOrderByIdOrderAndDataUser: function (order_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${order_model.table_name} tf INNER JOIN public."user" ts ON tf.buy_type_user_id = ts.user_id WHERE order_id=${order_id} LIMIT 1`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows[0]);
					reject("data not found");
				})
				.catch((err) => reject(err));
		});
	},
	removeOrderById: function (order_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`DELETE FROM ${order_model.table_name} WHERE order_id = '${order_id}'`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve("delete order sucess");
					reject("delete order not found");
				})
				.catch((err) => reject(err));
		});
	},
	getOrderByOrderPushnotificationWithData: function (order_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT tf.order_id , tf.res_id , tf.buy_type_user_id , tf.order_status , tf.food_order , tf.destination_location , tf.destination_location_name , tf.billing_cost , tf.sender_type_id , tf.order_complete , tf.order_restaurant, ts."location" , ts.res_name , ts.delivery_cost , tt.phone FROM ${order_model.table_name} tf INNER JOIN restaurant ts ON tf.res_id = ts.res_id INNER JOIN public."user" tt ON tf.buy_type_user_id = tt.user_id WHERE tf.order_id = ${order_id}`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows[0]);
					reject("data not found");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	getOrderByIdSender: function (sender_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT tf.order_id , tf.res_id , tf.buy_type_user_id , tf.order_status , tf.food_order , tf.destination_location , tf.destination_location_name , tf.billing_cost , tf.sender_type_id , tf.order_complete , tf.order_restaurant, ts."location" , ts.res_name , ts.delivery_cost , tt.phone FROM ${order_model.table_name} tf INNER JOIN public."restaurant" ts ON tf.res_id = ts.res_id INNER JOIN public."user" tt ON tf.buy_type_user_id = tt.user_id WHERE tf.sender_type_id = ${sender_id} ORDER BY tf.order_complete`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows);
					reject("data not found");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	getOrderByIdUser: function (user_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT tf.order_id , tf.res_id , tf.buy_type_user_id , tf.order_status , tf.food_order , tf.destination_location , tf.destination_location_name , tf.billing_cost , tf.sender_type_id , tf.order_complete , tf.order_restaurant, ts."location" , ts.res_name , ts.delivery_cost , tt.phone FROM ${order_model.table_name} tf INNER JOIN public."restaurant" ts ON tf.res_id = ts.res_id INNER JOIN public."user" tt ON tf.buy_type_user_id = tt.user_id WHERE tf.buy_type_user_id = ${user_id} ORDER BY tf.order_restaurant , tf.order_id DESC `
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows);
					reject("data not found");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	checkOrderRestaurantOrderSuccess: function (order_id) {
		console.log(order_id);

		return new Promise(async (resolve, reject) => {
			let result, error;
			({ error, result } = await functions.to(
				order_model.getOrderById(order_id)
			));

			if (error) reject(error);
			resolve(result.order_restaurant);
		});
	},
	checkSenderToSendOrder: function (sender_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${order_model.table_name} WHERE sender_type_id = ${sender_id} and order_complete = false `
				)
				.then((result) => {
					if (result.rowCount > 0) resolve("false");
					resolve("true");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	getOrderByIdResAndOrderSuccess: function (res_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT tf.order_id , tf.res_id , tf.buy_type_user_id , tf.order_status , tf.food_order , tf.destination_location , tf.destination_location_name , tf.billing_cost , tf.sender_type_id , tf.order_complete , tf.order_restaurant, ts."location" , ts.res_name , ts.delivery_cost FROM ${order_model.table_name} tf INNER JOIN restaurant ts ON tf.res_id = ts.res_id WHERE tf.res_id = ${res_id} and tf.order_restaurant = true ORDER BY order_id DESC`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows);
					reject("data not found");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	getOrderByIdResAndNotOrderSuccess: function (res_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT tf.order_id , tf.res_id , tf.buy_type_user_id , tf.order_status , tf.food_order , tf.destination_location , tf.destination_location_name , tf.billing_cost , tf.sender_type_id , tf.order_complete , tf.order_restaurant, ts."location" , ts.res_name , ts.delivery_cost FROM ${order_model.table_name} tf INNER JOIN restaurant ts ON tf.res_id = ts.res_id WHERE tf.res_id = ${res_id}  ORDER BY order_id ASC`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve(result.rows);
					reject("data not found");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	updateSenderIdOrder: function (order_id, sender_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`UPDATE ${order_model.table_name} SET sender_type_id = ${sender_id} WHERE order_id = ${order_id}`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve("update sender success");
					reject("update sender fail");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	updateOrderRestaurant: function (order_id, order_restaurant_value) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`UPDATE ${order_model.table_name} SET order_restaurant = ${order_restaurant_value} WHERE order_id = ${order_id}`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve("update order_restaurant success");
					reject("update order_restaurant fail");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	updateOrderComplete: function (order_id, order_complete_value) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`UPDATE ${order_model.table_name} SET order_complete = ${order_complete_value} WHERE order_id = ${order_id}`
				)
				.then((result) => {
					if (result.rowCount > 0) resolve("update order_complete success");
					reject("update order_complete fail");
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	history: function (req, res) {},
};

module.exports = order_model;
