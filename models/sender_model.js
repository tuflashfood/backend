const { pool } = require("../config/database_config");

const sender_model = {
	table_name_sender: "public.sender_queue",
	table_name_order: "public.order",
	statusReady: function (user_id) {
		return new Promise((resolve, reject) => {
			let current_datetime = new Date();
			let formatted_date =
				current_datetime.getDate() +
				"-" +
				(current_datetime.getMonth() + 1) +
				"-" +
				current_datetime.getFullYear() +
				" " +
				current_datetime.getHours() +
				":" +
				current_datetime.getMinutes() +
				":" +
				current_datetime.getSeconds();
			pool
				.query(
					`INSERT INTO ${sender_model.table_name_sender}(user_id , timestart)
            VALUES (${user_id},'${formatted_date}')`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve("Update Status Order Success");
				})
				.catch((err) => reject(err));
		});
	},
	confirmOrder: function () {
		return new Promise((resolve, reject) => {});
	},
	getByID: function (sender_queue_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${sender_model.table_name_sender} WHERE sender_queue_id = ${sender_queue_id}`
				)
				.then((data) => {
					if (data.rowCount > 0) {
						resolve(data.rows[0]);
					} else {
						resolve("not found");
					}
				})
				.catch((err) => reject(err));
		});
	},
	findSender: function () {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${sender_model.table_name_sender} ORDER BY timestart`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve(data.rows[0]);
				})
				.catch((err) => reject(err));
		});
	},
	senderNotReply: function (sender_queue_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`DELETE FROM ${sender_model.table_name_sender} WHERE sender_queue_id = ${sender_queue_id}`
				)
				.then((data) => {
					resolve("delete sender not reply sucess");
				})
				.catch((err) => reject(err));
		});
	},
	updateStatusOrderRestaurant: function (req, res) {},
	updateStatusOrderBuy: function (req, res) {},
	history: function (req, res) {},
};

module.exports = sender_model;
