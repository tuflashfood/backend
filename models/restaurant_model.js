const { pool } = require("../config/database_config");

const restaurant_model = {
	table_name: "public.restaurant",
	statusReady: function (res_id, statusUpdate) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`UPDATE ${restaurant_model.table_name} SET res_status = ${statusUpdate} WHERE res_id = ${res_id}`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve("Update Status Restuarant Success");
				})
				.catch((err) => reject(err));
		});
	},
	confirmOrder: function (res_id) {},
	getResById: function (res_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${restaurant_model.table_name} WHERE res_id=${res_id} LIMIT 1`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve(data.rows[0]);
					reject("not found restaurant");
				})
				.catch((err) => reject(err));
		});
	},
	getResByUserId: function (user_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${restaurant_model.table_name} WHERE res_type_user_id=${user_id} LIMIT 1`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve(data.rows[0]);
					reject("not found restaurant");
				})
				.catch((err) => reject(err));
		});
	},
	createRestaurant: function (
		res_user_id,
		location,
		res_status,
		menu,
		res_name,
		delivey_cost,
		cook_time
	) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`INSERT INTO ${
						restaurant_model.table_name
					} (res_type_user_id,location,res_status,menu,res_name,delivery_cost,cook_time)
        VALUES ('${res_user_id}',point${location},${res_status},'${JSON.stringify(
						menu
					)}','${res_name}', ${delivey_cost} , ${cook_time})`
				)
				.then((data) => resolve("Insert to database sucess"))
				.catch((err) => reject(err));
		});
	},
	getRestaurantOpen: function () {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${restaurant_model.table_name} WHERE res_status = true `
				)
				.then((data) => {
					if (data.rowCount > 0) {
						resolve(data.rows);
					} else {
						resolve("not_found");
					}
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	history: function (req, res) {},
};

module.exports = restaurant_model;
