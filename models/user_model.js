const { pool } = require("../config/database_config");
const functions = require("../function/function");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const user_model = {
	table_name: 'public."user"',
	getTestPass: function (password) {
		bcrypt.hash(password, saltRounds, function (err, hash) {
			console.log(hash);
		});
	},
	createUser: function (name, username, password, type, phone) {
		return new Promise(async (resolve, reject) => {
			let error, result;
			({ error, result } = await functions.to(
				pool.query(`SELECT * FROM public."user" WHERE username = '${username}'`)
			));
			if (result.rowCount > 0) reject("username duplicate");
			({ error, result } = await functions.to(
				pool.query(`SELECT * FROM public."user" WHERE name = '${name}'`)
			));
			if (result.rowCount > 0) reject("name duplicate");
			bcrypt.hash(password, saltRounds, function (err, hash) {
				// Store hash in your password DB.
				if (err) reject(err);
				pool
					.query(
						`INSERT INTO ${user_model.table_name} (name,username,password,type,phone) VALUES ('${name}','${username}','${hash}','${type}','${phone}')`
					)
					.then((data) => {
						resolve("Insert to database sucess");
					})
					.catch((err) => {
						reject(err);
					});
			});
		});
	},
	getByUsername: function (username) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${user_model.table_name} WHERE username='${username}' LIMIT 1`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve(data.rows[0]);
					//not found
					reject(data);
				})
				.catch((err) => reject(err));
		});
	},
	getByIdOrderGetDataRestaurant: function (order_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT tt.user_id, tt.name, tt.username, tt.password, tt.type, tt.phone, tt.firebase_token FROM public."order" tf 
          INNER JOIN restaurant ts ON tf.res_id = ts.res_id INNER JOIN ${user_model.table_name} tt ON ts.res_type_user_id = tt.user_id
          or tf.buy_type_user_id = tt.user_id WHERE tf.order_id = ${order_id}`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve(data.rows);
					reject("not found user");
				})
				.catch((err) => {
					console.log(err);

					reject(err);
				});
		});
	},
	getById: function (id_user) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`SELECT * FROM ${user_model.table_name} WHERE user_id='${id_user}' LIMIT 1`
				)
				.then((data) => {
					if (data.rowCount > 0) resolve(data.rows[0]);
					reject("not found user");
				})
				.catch((err) => reject(err));
		});
	},
	getByIdRes: function (res_id) {},
	checkPassword: function (pwd, hashpwd) {
		return new Promise((resolve, reject) => {
			bcrypt
				.compare(pwd, hashpwd)
				.then((same) => resolve(same))
				.catch((err) => reject(err));
		});
	},
	addTokenFirebase: function (token, user_id) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`UPDATE ${user_model.table_name} SET firebase_token = '${token}' WHERE user_id = ${user_id}`
				)
				.then((result) => {
					if (result.rowCount == 1) resolve("add token success");
				})
				.catch((err) => {
					reject("error not add token");
				});
		});
	},
};

module.exports = user_model;
