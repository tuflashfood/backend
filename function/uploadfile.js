const express = require("express");
const t = express();
const { upload, multer } = require("../middleware/uploadfile_middleware");

// import file picture to server
t.get("/upload", function (req, res) {
	res.writeHead(200, { "Content-Type": "text/html" });
	res.write('<form action="t" method="post" enctype="multipart/form-data">');
	res.write('<input type="file" name="image"><br>');
	res.write('<input type="submit">');
	res.write("</form>");
	return res.end();
});

// change buffer to base64 to show image
t.post("/img", function (req, res) {
	var uploads = upload.single("image");

	uploads(req, res, function (err) {
		console.log(req.file);
		if (err instanceof multer.MulterError) {
			// A Multer error occurred when uploading.
			// console.log(err);
			res.status(400).send("key not found");
		} else if (err) {
			// An unknown error occurred when uploading.
			if (err == "Please upload only images.") {
				res.status(400).send("Please upload only images");
			}
		}
		// Everything went fine.
		res.status(200).send("true");
	});

	// res.writeHead(200, { "Content-Type": "text/html" });
	// res.write(`<img src="data:image/jpeg;base64,${req.file.buffer.toString("base64").trim()}" alt="Italian Trulli">`);
	// console.log(req.file.buffer.toString("base64"));
});

// t.get("/", (req, res) => {
//   //send picture
//   res.sendfile("public/photo-1.jpeg");
// });

//read text form-data
t.post("/t", (req, res) => {
	var s = upload.array("image", 2);
	s(req, res, function (err) {
		console.log(req.files);
		console.log(req.body);
	});
	// var uploads = upload.single("image");
	// uploads(req, res, function(err) {
	//   console.log(req.file);
	// });
	res.send("eiei");
});

module.exports = { test: t };
