const moment = require("moment");
const functions = {
	to: function (promise) {
		return promise
			.then((result) => {
				return { error: null, result };
			})
			.catch((error) => {
				return { error };
			});
	},
	getResultToOrder: function (result) {
		let order = result.map((element) => {
			return functions.getDataToOrder(element);
		});
		return order;
	},
	getDataToOrder(element) {
		let {
			order_id,
			res_id,
			buy_type_user_id,
			order_status,
			food_order,
			destination_location,
			destination_location_name,
			billing_cost,
			sender_type_id,
			order_complete,
			order_restaurant,
			location,
			res_name,
			delivery_cost,
		} = element;

		return {
			order: {
				order_id,
				res_id,
				buy_type_user_id,
				order_status,
				food_order,
				destination_location,
				destination_location_name,
				billing_cost,
				sender_type_id,
				order_complete,
				order_restaurant,
			},
			location,
			res_name,
			delivery_cost,
		};
	},
	getResultToOrderSender: function (result) {
		let order = result.map((element) => {
			return functions.getDataToOrderSender(element);
		});
		return order;
	},
	getDataToOrderSender(element) {
		let {
			order_id,
			res_id,
			buy_type_user_id,
			order_status,
			food_order,
			destination_location,
			destination_location_name,
			billing_cost,
			sender_type_id,
			order_complete,
			order_restaurant,
			location,
			res_name,
			delivery_cost,
			phone,
		} = element;

		return {
			order: {
				order_id,
				res_id,
				buy_type_user_id,
				order_status,
				food_order,
				destination_location,
				destination_location_name,
				billing_cost,
				sender_type_id,
				order_complete,
				order_restaurant,
			},
			location,
			res_name,
			delivery_cost,
			phone,
		};
	},
	getTime: function () {
		moment.locale("th");
		return moment().format("l - LT");
	},
	sendMessageError: function (res, error) {
		const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
		res.status(statusCode);
		res.json({
			message: error.message,
		});
	},
	getArrayTokenFormUser: function (array_user) {
		let arrayToken = [];
		for (const {
			user_id,
			name,
			username,
			password,
			type,
			phone,
			firebase_token,
		} of array_user) {
			arrayToken.push(firebase_token);
		}
		return arrayToken;
	},
};

module.exports = functions;
