class Queue {
	constructor() {
		this.results = [];
	}

	toString() {
		this.results.forEach((element) => {
			console.log("sender_id " + element.sender_id + " ");
		});
	}

	push(sender_id, token, timestart) {
		let dataJson = {
			sender_id: sender_id,
			order_id: 0,
			timestart: timestart,
			time: undefined,
			await: false,
			array: {},
			token: token,
			answerSender: function () {
				return new Promise((resolve) => {
					this.time = setTimeout(() => {
						if (this.time._called) {
							resolve("no-reply");
						} else {
							//search user
							if (
								this.array.sender_id == this.sender_id &&
								this.array.order_id == this.order_id
							) {
								if (this.array.values === "accept") {
									resolve("accept");
								} else {
									resolve("unaccept");
								}
							} else {
								console.log("not found answerSender");
							}
						}
					}, 15000);
				});
			},
			onTimeout: function () {
				if (this.time) {
					if (this.time._called == false) {
						this.time._onTimeout();
						clearTimeout(this.time);
						this.time = undefined;
					}
					console.log("time out pass");
				}
			},
			addOrderSender: function (order_id) {
				this.order_id = order_id;
			},
			getToken: function () {
				return this.token;
			},
		};
		if (this.isEmpty()) {
			this.results.push(dataJson);
		} else {
			let index = this.results.findIndex((obj) => obj.timestart > timestart);
			if (index === -1) {
				this.results.push(dataJson);
			} else {
				this.results.splice(index, 0, dataJson);
			}
		}
	}

	getTime() {
		let str = "";
		this.results.forEach(
			(obj) => (str += obj.timestart + " " + obj.sender_id + " ")
		);
		console.log(str);
	}

	isEmpty() {
		return this.results && this.results.length == 0;
	}

	length() {
		return this.results.length;
	}

	unshift(request_sended) {
		// let array = [];
		if (!this.isEmpty()) {
			let value;
			let index = -1;
			if (request_sended && request_sended.length > 0) {
				for (const obj in this.results) {
					//data.order_id != this.results[obj].order_id && this.results[obj].await == false
					let text = request_sended.findIndex((data) => {
						return (
							data.order_id == this.results[obj].order_id ||
							this.results[obj].await == true
						);
						// ? true
						// : false;
					});

					if (text == -1) {
						index = obj;
						break;
					}
				}
			} else {
				index = this.results.findIndex((data) => data.await == false);
			}
			if (index == -1) return null;
			this.results[index].await = true;
			return this.results[index];
		} else {
			return null;
		}
	}

	removeQueue(sender_id) {
		let index = this.results.findIndex((obj) => obj.sender_id == sender_id);
		if (index !== -1) {
			return this.results.splice(index, 1)[0];
		} else {
			return null;
		}
	}

	addJsonArray(sender_id, order_id, json) {
		let index = this.results.findIndex(
			(obj) =>
				obj.sender_id == sender_id &&
				obj.order_id == order_id &&
				obj.await == true
		);

		if (index !== -1) {
			this.results[index].array = json;
			this.results[index].onTimeout();

			console.log("pass index");

			return true;
		} else {
			return null;
		}
	}

	findSenderInQueueById(sender_id) {
		let index = this.results.findIndex((obj) => obj.sender_id == sender_id);
		if (index !== -1) return true;
		return false;
	}
	// addOrderSender(sender_id, order_id) {
	//   let index = this.results.findIndex((obj) => obj.sender_id == sender_id);
	//   if (index !== -1) {
	//     this.results[index].order_id = order_id;
	//   }
	// }
}

module.exports = Queue;
