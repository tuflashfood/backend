PGDMP     :                    x            database_tu_ff    12.2 (Debian 12.2-2.pgdg100+1)    12.2     {           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            |           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            }           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            ~           1262    16558    database_tu_ff    DATABASE     ~   CREATE DATABASE database_tu_ff WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
    DROP DATABASE database_tu_ff;
                admin    false            �            1259    16559    order    TABLE     �  CREATE TABLE public."order" (
    order_id integer NOT NULL,
    res_id integer NOT NULL,
    buy_type_user_id integer NOT NULL,
    order_status json NOT NULL,
    food_order json NOT NULL,
    destination_location point NOT NULL,
    destination_location_name character varying NOT NULL,
    billing_cost integer NOT NULL,
    sender_type_id integer,
    order_complete boolean NOT NULL,
    order_restaurant boolean NOT NULL
);
    DROP TABLE public."order";
       public         heap    admin    false            �            1259    16565    order_order_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.order_order_id_seq;
       public          admin    false    202                       0    0    order_order_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.order_order_id_seq OWNED BY public."order".order_id;
          public          admin    false    203            �            1259    16580 
   restaurant    TABLE     0  CREATE TABLE public.restaurant (
    res_id integer NOT NULL,
    res_type_user_id integer NOT NULL,
    location point NOT NULL,
    res_status boolean NOT NULL,
    menus json NOT NULL,
    res_name character varying(100) NOT NULL,
    delivery_cost integer NOT NULL,
    cook_time integer NOT NULL
);
    DROP TABLE public.restaurant;
       public         heap    admin    false            �            1259    16586    restaurant_res_id_seq    SEQUENCE     �   CREATE SEQUENCE public.restaurant_res_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.restaurant_res_id_seq;
       public          admin    false    204            �           0    0    restaurant_res_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.restaurant_res_id_seq OWNED BY public.restaurant.res_id;
          public          admin    false    205            �            1259    16595    user    TABLE     ?  CREATE TABLE public."user" (
    user_id integer NOT NULL,
    name character varying(50) NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(100) NOT NULL,
    type character varying(50) NOT NULL,
    phone character varying(10) NOT NULL,
    firebase_token character varying(200)
);
    DROP TABLE public."user";
       public         heap    admin    false            �            1259    16598    user_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.user_user_id_seq;
       public          admin    false    206            �           0    0    user_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.user_user_id_seq OWNED BY public."user".user_id;
          public          admin    false    207            �
           2604    16600    order order_id    DEFAULT     r   ALTER TABLE ONLY public."order" ALTER COLUMN order_id SET DEFAULT nextval('public.order_order_id_seq'::regclass);
 ?   ALTER TABLE public."order" ALTER COLUMN order_id DROP DEFAULT;
       public          admin    false    203    202            �
           2604    16603    restaurant res_id    DEFAULT     v   ALTER TABLE ONLY public.restaurant ALTER COLUMN res_id SET DEFAULT nextval('public.restaurant_res_id_seq'::regclass);
 @   ALTER TABLE public.restaurant ALTER COLUMN res_id DROP DEFAULT;
       public          admin    false    205    204            �
           2604    16606    user user_id    DEFAULT     n   ALTER TABLE ONLY public."user" ALTER COLUMN user_id SET DEFAULT nextval('public.user_user_id_seq'::regclass);
 =   ALTER TABLE public."user" ALTER COLUMN user_id DROP DEFAULT;
       public          admin    false    207    206            s          0    16559    order 
   TABLE DATA           �   COPY public."order" (order_id, res_id, buy_type_user_id, order_status, food_order, destination_location, destination_location_name, billing_cost, sender_type_id, order_complete, order_restaurant) FROM stdin;
    public          admin    false    202   �"       u          0    16580 
   restaurant 
   TABLE DATA              COPY public.restaurant (res_id, res_type_user_id, location, res_status, menus, res_name, delivery_cost, cook_time) FROM stdin;
    public          admin    false    204   �%       w          0    16595    user 
   TABLE DATA           `   COPY public."user" (user_id, name, username, password, type, phone, firebase_token) FROM stdin;
    public          admin    false    206   �.       �           0    0    order_order_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.order_order_id_seq', 184, true);
          public          admin    false    203            �           0    0    restaurant_res_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.restaurant_res_id_seq', 1, false);
          public          admin    false    205            �           0    0    user_user_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.user_user_id_seq', 5, true);
          public          admin    false    207            �
           2606    16608    order order_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (order_id);
 <   ALTER TABLE ONLY public."order" DROP CONSTRAINT order_pkey;
       public            admin    false    202            �
           2606    16614    restaurant restaurant_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.restaurant
    ADD CONSTRAINT restaurant_pkey PRIMARY KEY (res_id);
 D   ALTER TABLE ONLY public.restaurant DROP CONSTRAINT restaurant_pkey;
       public            admin    false    204            �
           2606    16620    user user_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
       public            admin    false    206            �
           2606    16622    user user_username_key 
   CONSTRAINT     W   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);
 B   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_username_key;
       public            admin    false    206            �
           2606    16623 !   order order_buy_type_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_buy_type_user_id_fkey FOREIGN KEY (buy_type_user_id) REFERENCES public."user"(user_id) NOT VALID;
 M   ALTER TABLE ONLY public."order" DROP CONSTRAINT order_buy_type_user_id_fkey;
       public          admin    false    206    202    2799            �
           2606    16638    order order_res_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_res_id_fkey FOREIGN KEY (res_id) REFERENCES public.restaurant(res_id) NOT VALID;
 C   ALTER TABLE ONLY public."order" DROP CONSTRAINT order_res_id_fkey;
       public          admin    false    204    2797    202            �
           2606    16648 +   restaurant restaurant_res_type_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.restaurant
    ADD CONSTRAINT restaurant_res_type_user_id_fkey FOREIGN KEY (res_type_user_id) REFERENCES public."user"(user_id) NOT VALID;
 U   ALTER TABLE ONLY public.restaurant DROP CONSTRAINT restaurant_res_type_user_id_fkey;
       public          admin    false    206    2799    204            s   y  x��M��@���_�P�3��\�{؋W��Nmh��4�H)xX�? ~]�JQA\V��f~���ݸ�m�J$�&�w��w��M�#�8N<����0��K��wqL�3㈘G�0��Ө�^v�4�S?��$d�t��=����=7����4���x�y�%VK�h��I�E)��Rĩ����3��uA=�Q��Y@Mu��'<�N#��A�$~���_�Z�se<�ϯW-�;��[7l�v)!=���iYw�ߤ�!�<?I����Jn^��%H����5�I�Z�pYkb�^J�L��+����"�ú`�����)�H!T|)�B*̥FU{j��;DRv_��+�\�N�!v�m���Z8h�����-�L�3�B懣 Ua�~e���ר��������K�&X{uJ{�K��q��>�sTy��x(��|�E�FȖ�E���F�X]Z��Q~��$�|��y8��Q�7��cl�R�9�������T��� �æU1I�H�a�5#���m�D(�AD���	"m�m�fCj�CƩ	#z��j�ԪX���T�v���!��Z��~ضp�&����:�
��b�L�ĈQ�����9���:��/~V��      u   	  x��Z]o��}v~�@O-`�d�޼�٢�â@�ݢH�����$���l��
���Ou6��+��2����Jg��e�q�(�"�@�����8�ܙ���K?+��^Y�l���啕�k+卍�/u�������6JwJq4��iq������Q7���QϞǳ�8:�gG���%}���h�V���@؄�u�%E� �*�B�kH�)���kq4��c� f��_>@�Ƴ~��g)$:���8z9�8�;V�y�F�5e��[��k�$�c�D���^G�E����[� ��-y�i�������J|T����1�@�Kv$V�����C���X�\�9b���1�����>ΰ�#H�X��א0��C�N0����$6�.^/��#z_��;Xp���)-�l��l{��ҝ�%�{��qx�C���uл�,�ܦ��p�r[���r[u����M��W��&޹��Բ�b�o����=�v[�뻶�X��-�m��n��ZpYC%Bзr��\����Ȝ��b_c4�C	ZK�a,��}9R,˵;{��v�j4�����~޳�n�F :=�����;�p��c>��4���}k�zز:f�����+���OJ��|����A�x��nk�m
�뎽��}c���7�b��^G��B�Z>7G��{�����l��o�ѰR��g��'n3+e�����6	�)LK28��6g���L1@�NDd
+w�V/$4�|RMo�c�J��+�I��*�p����9�G���ǰH��k�a�G�>�Hx����l��,�Q�=r[ζ�3R���M�������q�H��%���:�t���}�m��V���gY��#|;��Q�>U�^3\�_�p��Da7<��Im(��h��#2kQ*��
ޣ�Z�jj%�
�?��c��M���v�������1�b��O��.�d
�� �� J��'�$�w��8�+�j�ȌzIz�Y�4�ms��X�-{���p����Z����[KE�z��>�C&h�'o{��5O�.�V�`����nU���r�h`���u�`�0�Ji�����M�[����i��vD�P�QgUͭ���&��~еL�������HO^��T$N���
H�`���rL���`��S�UZ��s�R�LC2L�XI\�*�̳�n�e�R!=HX�L��O� ��4��#���L>��N�ZǱ��%���FQ��Y0�e`���8-�ƚncǤk�E�LB���yn�s�@��Ôp����<Ie�*�^S`HU�;ٽ8�[K��JX���TA1"sv>2p��uT� I��M�/�Q����\���;V3�b_
����_�-�I���g����H�f�|�/A�S�0�, F�A)4� H<$����Յ=d�����&��v���#��Znk���3Bg׹F|�l'Q�0��*w�\hl��\�J!��K�3:�tC2*I.��_?��Z�/��,��J0`⭽�T���c�����0X��^9��6�G=Jj�82�k���/�.)�.(�t�3�,8,TS&'��USM��U�8q��PV�PT�ٰ\B S���a��x�+R̈́$�(�)��z�^���uv���IP|H)0����_S)$Ds���}�������X���j[v/^k�0/
g&�71M32Ì�[�Kzޛs*V|9��pQu��_��FE�
M�YvQ��?K.�Vw�I�����tSEae�����j���J����+d������칁 '��Ը5@%�N��sv�0�g����\�Z5 k���dX� �%���	��Cqyql֡ì�����+$A��<��3��ШP*j��6x��+I<�}�m>f������S�$f��#�C�e���*Y?xV�lܥQS�J��k���0n|T�M��[��{��M˞�v=�_w�mZ�n��;���g���j���_��q�O�b9�� �Ր��Ŧ<
��p������C�M�T��QZgn@¹2P���[�Ҁ�+�J�V�9���92U�>�*b�.3�ab-�T���I�A{;��6�aVf��)M��P~��1ElȞƼ`t8<1V�=�|��b�2Q��sR���k{~��L�,M��1�`F��1¨.����Rj��n9q/)����4�z^}�Ϡa��tLi����Cjy����x��6�1��#y�Y��L�Ig���V]�������ֲR�36^��5�#]��-g2֛�9�bt����U.ǌ��8��:��ϋ�vT���s�i�^�h����?)���}f�Kj��p���o������҅���}�֭�@Y      w   �  x���Ko�`�5��Y������DA��tҤ>�������$������=9ON^�`H6-���	��������5_�i#�8fɶ�VshM�.��_�)��>�j=r �QN���k(�tMX���ߦ�6��$Es,E"�xd���۶�X/�+�����a�a�X�~�x��$yN���5���St��0V,����^oʮ�$�=]\=���lpK@�i���T��P�bkR�*�7��M�<8˒0f�4~�C��\�Ɗ.@,ٸ�b�T�g��i�y��9����+ڛN�4��$���D��:ߝ"���O�x(C���)k�M({K,��Gs����qE���a�`�n�[%�]N�s6nм9��H8����J���>Uا�y�=��e��}���8.����nw�.]���خQ
�Z��� ��}K>~��ro*﯀#A����@ �&,�V�;?g������yAQ��ؒ�     