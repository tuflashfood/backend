const axios = require("axios");
const querystring = require("querystring");
require("dotenv").config();
// const sender_model = require("../models/sender_model");

const push_notification = {
	sendmessage: function (token, datas, titile, body) {
		return axios.post(
			"https://fcm.googleapis.com/fcm/send",
			{
				registration_ids: token,
				priorty: "high",
				notification: {
					title: titile,
					body: body,
					click_action: "OPEN_DETAIL_ACTIVITY",
				},
				data: datas,
				// data: {
				//   story_id: "story_12346",
				// },
			},
			{
				headers: {
					Authorization: "key=" + process.env.KEY_SERVER,
					"Content-Type": "application/json",
				},
			}
		);
	},
};

module.exports = push_notification;
