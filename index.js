const express = require("express");
const app = express();
const helmel = require("helmet");
const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");
const fucntions = require("./function/function");
const push_notification = require("./config/firebase_config");
const router = require("./routes/router");
const moment = require("moment");
require("dotenv");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan("combined"));
app.use(helmel());

app.use("/api", router);

app.get("/tt", async function (req, res) {
	let result, error;
	({ error, result } = await fucntions.to(name()));
	console.log("error " + error);
	console.log("result " + result);
	({ error, result } = await fucntions.to(name1()));
	console.log("error " + error);
	console.log("result " + result);
	res.send("eiei");
});

function name() {
	return new Promise((resolve, reject) => {
		resolve("eiei");
	});
}

function name1() {
	return new Promise((resolve, reject) => {
		reject("eiei");
	});
}

app.get("/push", function (req, res) {
	moment.locale("th");
	let t = moment().format("l - LT");
	push_notification
		.sendmessage(
			[
				"e4BOmXz4TKCmHWfNsSbg83:APA91bEweBLhk8TwPDkcKLSFnjJ8rxR-Dydz3B6v7XFF31VVs8xk01lORhtKj9fHiezdaFm9dsXcsYVWfQj9Q5_6zqEEqj7agMdvkThxBaJvOJd7LbErrKiinf_ER6juHyG-X2qO0xtC",
				"fgpqjlrjQsGQzY0KLj3ivz:APA91bHqbXIzgSVwU7YQAtjc9825Q4gPKmcd5qcEHfTTNxMMP_McnqCsBbTxj8HsRdLKxbcDGPydFl9OkJMsBa20r4DFGCp0BgipCTuELCDmlLWojHg9x40lwTrWY_O3AIbFI7PB3dAs",
			],
			{
				data: {
					order: {
						order_id: 2,
						res_id: 1,
						buy_type_user_id: 2,
						order_status: {
							order: { value: true, time: "12/05/2020 - 19:31" },
							prepare_products: { value: false, time: "" },
							shipping: { value: false, time: "" },
							delivered: { value: false, time: "" },
						},
						food_order: [
							{
								amount: "1",
								total_price: "150",
								product_notes: "",
								price: "150",
								menu_name: "cakechocolate",
								key: "cakechocolate",
							},
						],
						destination_location: {
							x: 14.0734791,
							y: 100.6059566,
						},
						destination_location_name:
							"อาคารบรรยายเรียนรวม 2 ตำบล คลองหนึ่ง อำเภอคลองหลวง ปทุมธานี 12120 ประเทศไทย",
						billing_cost: 150,
						sender_type_id: null,
					},
					location: {
						x: 14.073313,
						y: 100.606438,
					},
					res_name: "moserper",
					delivery_cost: 20,
					phone: "0231243212",
				},
				time: t,
			},
			"title",
			"body"
		)
		.then((result) => {
			console.log(result.data);
		})
		.catch((err) => {
			console.log(err);
		});
	res.sendStatus(200);
});

app.get("/", function (req, res) {
	res.json({
		message: "hello world",
	});
});

// send message when url not found in server
app.use((req, res, next) => {
	const error = new Error(`Not found - ${req.originalUrl}`);
	res.status(404);
	next(error);
});

app.use((error, req, res, next) => {
	const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
	res.status(statusCode);
	res.json({
		message: error.message,
	});
});

app.listen(4000, function () {
	console.log("Example app listening on port 4000!");
});
