const router = require("express").Router();
const orderController = require("../controllers/order_controller");
const user_model = require("../models/user_model");
const order_model = require("../models/order_model");
const restaurantController = require("../controllers/restaurant_controller");
// const senderController = require("../controllers/sender_controller");
const user_controller = require("../controllers/user_controller");
const userAuth_controller = require("../controllers/userAuth_controller");
const Queue = require("../function/queue");
// const userAuth_middleware = require("../middleware/userAuth_middleware")
//   .userAuth_middleware;
const push_notification = require("../config/firebase_config");
const functions = require("../function/function");
const queue = new Queue();
// queue.push(
// 	3,
// 	"exU8-6bvTTS01iOovy9Xiz:APA91bFo0vz9BJb1IJA0W0RIsCdfQCVnurw77SiFMz7NWPKOrQYKHnlIRXWpXOjFblspD2SMw0caNKPVpqqyQMMnLdr4d5oAf57Xf0lKNzGmchJIExldVwNBzbe7qSXbqwxq6n0iIT2F",
// 	"15:13"
// );
// console.log(queue);

//user
router.post("/user/createuser", user_controller.createUser);
router.post("/user/login", userAuth_controller.login);
router.get("/user/checktoken", userAuth_controller.checkToken);
router.get("/user/getbyid/:id", user_controller.getById);
router.get(
	"/user/test/:order_id",
	user_controller.getByIdOrderGetDataRestaurant
);
router.post("/user/addtokenfibase", user_controller.addTokenFirebase);
router.post("/user/currentlocation", user_controller.getLocation);

// restaurant
router.post("/restaurant/searchsender", async function (req, res) {
	if (!req.body.order_id)
		res.status(401).json({ message: "not found sender_id" });
	console.log("QUEUE ----");
	console.log(queue.length);

	let sender = [];
	let resultArray,
		senderAgree,
		error,
		result,
		dataNotification,
		phone,
		arrayDataRestaurantandUser;
	let searchOrder = req.body.order_id;
	console.log("order id " + searchOrder + "----");
	res.status(200).json({ message: "search sender" });

	({ error, result } = await functions.to(
		user_model.getByIdOrderGetDataRestaurant(searchOrder)
	));

	if (error) {
		console.log("not found restaurant and user");
		return;
	}
	if (result) arrayDataRestaurantandUser = result;

	if (!queue.isEmpty()) {
		// search order send
		({ error, result } = await functions.to(
			orderController.getOrderByOrderPushnotificationWithData(searchOrder)
		));
		if (error) {
			console.log("server error" + error);
			return;
		}

		if (result) {
			dataNotification = functions.getDataToOrderSender(result);
			// console.log("dataNotification " + JSON.stringify(dataNotification));

			//get phone user
			({ error, result } = await functions.to(
				user_model.getById(result.buy_type_user_id)
			));
			if (result) phone = result.phone;
		}

		//search sender
		while (true) {
			// search sender not duplicate and sender ready send
			resultArray = queue.unshift(sender);
			// not found sender
			if (resultArray == null) {
				console.log("not found sender");
				break;
			}
			// push notification to sender
			console.log(dataNotification);

			push_notification
				.sendmessage(
					[resultArray.getToken()],
					{ data: dataNotification, time: functions.getTime() },
					"การจัดส่งสินค้า",
					"มีเวลาตอบ 30 วินาทีเท่านั้น"
				)
				.then((result) => {
					console.log(
						result + "push notification to sender success ask want to send"
					);
				})
				.catch((err) => {
					// console.log("push notification to sender fail ask want to send");
					console.log(
						err + "push notification to sender fail ask want to send"
					);
				});
			resultArray.addOrderSender(searchOrder);
			({ error, result } = await functions.to(resultArray.answerSender()));
			console.log("await sender");
			if (result) {
				console.log("result sender answer " + result);
				//found sender and break loopasdfsadfuuu
				if (result == "accept") {
					senderAgree = queue.removeQueue(
						resultArray.sender_id,
						resultArray.order_id
					);
					console.log(queue);

					console.log("break");
					break;
				} else if (result == "unaccept") {
					//set await array
					resultArray.onTimeout();
					resultArray.await = false;
					resultArray.array = {};
					sender.push({
						sender_id: resultArray.sender_id,
						order_id: resultArray.order_id,
					});
				} else {
					//sender not reply
					queue.removeQueue(resultArray.sender_id, resultArray.order_id);

					let tokenSender;
					({ error, result } = await functions.to(
						user_model.getById(resultArray.sender_id)
					));

					if (error) {
						console.log("sender not reply and error not found sender" + error);
					}

					tokenSender = result.firebase_token;
					// push notification update status away sender
					push_notification
						.sendmessage(
							[tokenSender],
							{ messageCancelQueueSender: "คิวของคุณถูกยกเลิก" },
							"คิวของคุณถูกยกเลิก",
							"เนื่องจากคุณไม่ตอบออเดอร์ที่เข้ามาใหม่"
						)
						.then((result) => {
							console.log("คิวของคุณถูกยกเลิก");
							console.log(result.data);
						})
						.catch((err) => {
							console.log(err);
						});
				}
			} else {
				console.log("server 500");
			}
		}
	}

	// not found sender
	if (resultArray == null || resultArray == undefined) {
		// remove order
		console.log(searchOrder + " searchOrder");
		//get token res , user

		if (error) console.log("not found user (get data token restaurant)");
		//push notification to res and user
		push_notification
			.sendmessage(
				functions.getArrayTokenFormUser(arrayDataRestaurantandUser),
				{ messageResCancel: "message order cencle" },
				"ออเดอร์ที่ " + searchOrder + " ถูกยกเลิก",
				"เนื่องจากไม่มีคนส่งสินค้า"
			)
			.then((result) => {
				console.log(result.data);
			})
			.catch((err) => {
				console.log(err);
			});
		({ error, result } = await functions.to(
			order_model.removeOrderById(searchOrder)
		));
		if (error) console.log("remove order fail (not found sender)");
	} else {
		//add sender in database order
		console.log("sender accept");
		console.log(senderAgree.order_id + " senderAgree order");
		order_model
			.updateSenderIdOrder(senderAgree.order_id, senderAgree.sender_id)
			.then((result) => {
				console.log("add sender send order success");
			})
			.catch((err) => {
				console.log("add sender send order fail");
			});

		//remove sender in queue
		if (queue.removeQueue(senderAgree.sender_id) != null) {
			console.log("remove sender in queue success");
		} else {
			console.log("remove sender in queue fail");
		}
	}
	console.log("end search sender");
});
router.get(
	"/restaurant/getdatauserid/:user_id",
	restaurantController.getResByUserId
);
router.get("/restaurant/getdata/:res_id", restaurantController.getResById);
router.get("/restaurant/open", restaurantController.getRestaurantOpen);

//order
router.post("/order/createorder", orderController.createOrder);
router.post("/order/updatestatusorder", orderController.updateStatusOrder);
router.get(
	"/order/getorderbyidsender/:sender_id",
	orderController.getOrderByIdSender
);
router.get(
	"/order/checkorderrestaurantordersuccess/:order_id",
	orderController.checkOrderRestaurantOrderSuccess
);
router.get(
	"/order/getorderbyidresandordersuccess/:res_id",
	orderController.getOrderByIdResAndOrderSuccess
);
router.get(
	"/order/getorderbyidresandnotordersuccess/:res_id",
	orderController.getOrderByIdResAndNotOrderSuccess
);
router.post(
	"/order/updateorderrestaurant",
	orderController.updateOrderRestaurant
);
router.post("/order/updateordercomplete", orderController.updateOrderComplete);
router.get(
	"/order/getorderbyidanddatauser/:order_id",
	orderController.getOrderByIdOrderAndDataUser
);
router.get(
	"/order/removeorderrestaurant/:order_id",
	orderController.removeOrderSender
);
router.post("/order/updatesenderidorder", orderController.updateSenderIdOrder);
router.get(
	"/order/getorderbyiduser/:user_id",
	orderController.getOrderByIdUser
);
//sender add and remove queue
router.post(
	"/sender/statussender",
	function (req, res, next) {
		if (req.body.status == null && req.body.sender_id == null) {
			const error = new Error("input status null");
			res.status(401);
			next(error);
		}
		if (req.body.status == "add" || req.body.status == "remove") {
			next();
		} else {
			const error = new Error("input status fault");
			res.status(401);
			next(error);
		}
	},
	function (error, req, res, next) {
		console.log("error");
		functions.sendMessageError(res, error);
	}
);

router.post("/sender/statussender", async function (req, res, next) {
	let status = req.body.status;
	let sender_id = req.body.sender_id;
	let result, error, token;
	let value = false;
	console.log(status + " status");
	console.log(sender_id + " sender_id");

	if (status == "add") {
		({ error, result } = await functions.to(
			order_model.checkSenderToSendOrder(sender_id)
		));
		if (error) {
			console.log("error checkSenderToSendOrder " + error);
		}
		console.log(result + " result");

		if (result == "true") {
			({ error, result } = await functions.to(
				user_controller.getByIdWithData(sender_id)
			));
			if (result) {
				token = result.firebase_token;
				queue.push(sender_id, token, functions.getTime);
				value = true;
				console.log("add sender " + sender_id + " to queue");
			}
		} else {
			res.status(401).json({ message: "กรุณาส่งสินค้าให้เสร็จ" });
			return;
		}
	} else if (status == "remove") {
		if (queue.removeQueue(sender_id) != null) value = true;
	}
	console.log(queue.length());

	if (value) console.log(`${status} status sender success`);
	else console.log(`${status} not found sender fail`);

	if (value)
		res.status(200).json({ message: `${status} status sender success` });
	else res.status(404).json({ message: `${status} not found sender` });
});

router.post(
	"/sender/answertosend",
	function (req, res, next) {
		if (!(req.body.sender_id && req.body.order_id && req.body.value)) {
			const error = new Error("input status null");
			res.status(401);
			next(error);
		}
		next();
	},
	function (error, req, res, next) {
		functions.sendMessageError(res, error);
	}
);

router.post("/sender/answertosend", function (req, res, next) {
	console.log("3");
	let json = {
		sender_id: parseInt(req.body.sender_id),
		order_id: parseInt(req.body.order_id),
		values: req.body.value,
	};

	let result = queue.addJsonArray(json.sender_id, json.order_id, json);

	if (result) {
		console.log("answerToSender YES ");
		res.status(200).json({ message: "send answer success" });
	} else {
		console.log("answerToSender NO ");
		res.status(404).json({ message: "bad data not found in queue" });
	}
});

router.get("/sender/checkqueue/:sender_id", function (req, res) {
	if (!req.params.sender_id)
		res.status(401).json({ message: "error not found input" });

	const result = queue.findSenderInQueueById(req.params.sender_id);
	console.log(result + "adsfasdf");

	if (result) res.status(200).json({ message: "true" });
	else res.status(200).json({ message: "false" });
});

module.exports = router;
