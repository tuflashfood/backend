const sender_model = require("../models/sender_model");
const user_model = require("../models/user_model");
const order_model = require("../models/order_model");

module.exports = {
  statusReady: function(req, res) {
    sender_model.statusReady(req.params.user_id);
  },
};
