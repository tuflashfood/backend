const user = require("../models/user_model");
const functions = require("../function/function");
const google_map_model = require("../models/google_map_model");
module.exports = {
	createUser: async (req, res) => {
		let result, error;
		let resultPhone = req.body.phone.replace(/-/g, "");

		user
			.createUser(
				req.body.name,
				req.body.username,
				req.body.password,
				"user",
				resultPhone
			)
			.then((result) => {
				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(500).json({ message: err });
			});
	},
	getByIdWithData: (id) => {
		return user.getById(id);
	},
	getByIdOrderGetDataRestaurant: (req, res) => {
		user
			.getByIdOrderGetDataRestaurant(req.params.order_id)
			.then((result) => {
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: "Internal error please try again" });
			});
	},
	getById: (req, res) => {
		user
			.getById(req.params.id)
			.then((result) => {
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: "Internal error please try again" });
			});
	},
	addTokenFirebase: (req, res) => {
		user
			.addTokenFirebase(req.body.token, req.body.user_id)
			.then((result) => {
				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(404).json({ message: "Internal error please try again" });
			});
	},
	getLocation: (req, res) => {
		let destination_location_name;
		let destination_location = req.body.latlong;
		res.status(200).json({
			message:
				"อาคารบรรยายเรียนรวม 2 ตำบล คลองหนึ่ง อำเภอคลองหลวง ปทุมธานี 12120 ประเทศไทย",
		});
		// google_map_model
		// 	.getNameLatLongToAddress(destination_location)
		// 	.then((result) => {
		// 		let obj = result.data;
		// 		destination_location_name = obj.results[0].formatted_address;
		// 		res.status(200).json({ message: destination_location_name });
		// 	})
		// 	.catch((err) => {
		// 		res.status(404).json({ error: "google map api error" });
		// 	});
	},
};
