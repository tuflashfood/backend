const restaurant_model = require("../models/restaurant_model");
const order_model = require("../models/order_model");
module.exports = {
	getResByUserId: function (req, res) {
		restaurant_model
			.getResByUserId(req.params.user_id)
			.then((result) => {
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	getResById: function (req, res) {
		restaurant_model
			.getResById(req.params.res_id)
			.then((result) => {
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	getRestaurantOpen: function (req, res) {
		restaurant_model
			.getRestaurantOpen()
			.then((result) => {
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
};
