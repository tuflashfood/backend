const jwt = require("jsonwebtoken");
const middleware = require("../middleware/userAuth_middleware")
	.userAuth_middleware;
const user_model = require("../models/user_model");
const private_key = middleware.private_key;

module.exports = {
	login: function (req, res) {
		user_model
			.getByUsername(req.body.username)
			.then((result) => {
				user_model
					.checkPassword(req.body.password, result.password)
					.then((matched) => {
						if (matched) {
							let {
								user_id: user_id,
								name: name,
								username: username,
								type: type,
								phone: phone,
								firebase_token: firebase_token,
							} = result;
							res
								.status(200)
								.json({ user_id, name, username, type, phone, firebase_token });
						} else {
							console.log("err login");
							res.status(404).json({ errors: "Incorrect password" });
						}
					})
					.catch((err) => {
						console.log({ errors: "bcrypt compare catch" });
						console.log(err);
						res.status(500).json({ errors: "server error" });
					});
			})
			.catch((err) => {
				res.status(401).json({ errors: "Incorrect username" });
			});
	},
	checkToken: function (req, res) {
		console.log(req.user);
		res.sendStatus(200);
	},
	logout: function (req, res) {
		res.cookie("token", "", { httpOnly: true }).status(200);
	},
};
