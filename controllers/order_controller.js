const order_model = require("../models/order_model");
const restaurant_model = require("../models/restaurant_model");
const push_notification = require("../config/firebase_config");
const user_model = require("../models/user_model");
const functions = require("../function/function");
const moment = require("moment");
module.exports = {
	// searchResturantNear: function(req, res) {},
	// buyOrder: function(req, res) {},
	confirmOrder: function (req, res) {},
	history: function (req, res) {},
	updateStatusOrder: function (req, res) {
		console.log(req.body.status + " updateStatusOrder");

		order_model
			.updateStatusOrder(req.body.order_id, JSON.parse(req.body.status))
			.then((result) => {
				console.log(result);
				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(404).json({ message: "Updata Status Failed" });
			});
	},
	createOrder: async function (req, res) {
		let textToJson = [];
		if (typeof req.body.food_order == "string") {
			textToJson.push(req.body.food_order);
		} else {
			textToJson = req.body.food_order;
		}
		textToJson = textToJson.map((element) => {
			element = element.replace(/=/g, ":");
			element = element.replace(/=/g, ":");
			element = element.replace(/:,/g, ':"",');
			element = element.replace(/,(\s+)/g, ",");
			element = element.replace(/[\w\sก-๙]+/g, '"$&"');
			// remove "1" => 1 ex amount = "1" => amount = 1
			element = element.replace(/("(\d+)")/g, "$2");
			element = JSON.parse(element);
			delete element["menu_description"];
			return element;
		});

		// console.log(JSON.stringify(textToJson));
		let timeOrder = moment(req.body.time, "DD MMM HH:mm", "th").format(
			"DD/MM/YYYY - HH:mm"
		);
		let orderStatus = {
			order: { value: true, time: `${timeOrder}` },
			prepare_products: {
				value: false,
				time: "",
			},
			shipping: { value: false, time: "" },
			delivered: { value: false, time: "" },
		};

		console.log(req.body.res_id);
		console.log(req.body.buy_type_user_id);
		console.log(orderStatus);
		console.log(textToJson);
		console.log(req.body.billing_cost);
		console.log(req.body.destination_location);
		console.log(req.body.destination_location_name);
		console.log("end create order");

		let tokenRes, result, error, dataPushNotification, restaurant_id;
		({ error, result } = await functions.to(
			order_model.createOrder(
				req.body.res_id,
				req.body.buy_type_user_id,
				orderStatus,
				textToJson,
				req.body.billing_cost,
				req.body.destination_location,
				req.body.destination_location_name
			)
		));
		if (result) {
			res.status(200).json({ message: result });
		} else {
			res.status(404).json({ error: err });
		}

		({ error, result } = await functions.to(
			order_model.getOrderByIdResIdUserOrderStatus(
				req.body.res_id,
				req.body.buy_type_user_id,
				orderStatus
			)
		));
		moment.locale("th");
		let currentTime = moment().format("l - LT");
		if (error)
			console.log("not found data order (pushnotificatoin to restaurant)");
		if (result) {
			dataPushNotification = functions.getDataToOrder(result);
			// console.log(dataPushNotification);
			({ error, result } = await functions.to(
				restaurant_model.getResById(dataPushNotification.order.res_id)
			));
			if (error) console.log("not found restaurant");
			user_model
				.getById(result.res_type_user_id)
				.then((result) => {
					tokenRes = result.firebase_token;
					console.log(result);

					push_notification
						.sendmessage(
							[tokenRes],
							{ data: dataPushNotification, time: currentTime },
							"ร้านอาหาร",
							"มีออเดอร์เข้ามาใหม่"
						)
						.then((result) => {
							console.log("push notification to restaurant success");
						})
						.catch((err) => {
							console.log("push notification to restaurant fail");
						});
				})
				.catch((err) => {
					console.log("not found user (pushnotificatoin to restaurant)");
				});
		}
		console.log("end create order");
	},
	getOrderByOrderPushnotificationWithData(order_id) {
		return order_model.getOrderByOrderPushnotificationWithData(order_id);
	},
	getOrderByIdUser: function (req, res) {
		order_model
			.getOrderByIdUser(req.params.user_id)
			.then((result) => {
				result = functions.getResultToOrder(result);
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	getOrderByIdSender: function (req, res) {
		order_model
			.getOrderByIdSender(req.params.sender_id)
			.then((result) => {
				result = functions.getResultToOrderSender(result);
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	getOrderByIdOrderAndDataUser: function (req, res) {
		order_model
			.getOrderByIdOrderAndDataUser(req.params.order_id)
			.then((result) => {
				console.log(result.firebase_token);

				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ error: err });
			});
	},
	getOrderById: function (req, res) {
		order_model
			.getOrderById(req.params.id)
			.then((result) => {
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	getOrderByIdResAndOrderSuccess: function (req, res) {
		order_model
			.getOrderByIdResAndOrderSuccess(req.params.res_id)
			.then((result) => {
				result = functions.getResultToOrder(result);
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	getOrderByIdResAndNotOrderSuccess: function (req, res) {
		order_model
			.getOrderByIdResAndNotOrderSuccess(req.params.res_id)
			.then((result) => {
				result = functions.getResultToOrder(result);
				res.status(200).json(result);
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	checkOrderRestaurantOrderSuccess: function (req, res) {
		order_model
			.checkOrderRestaurantOrderSuccess(req.params.order_id)
			.then((result) => {
				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(400).json({ message: err });
			});
	},
	updateSenderIdOrder: function (req, res) {
		order_model
			.updateSenderIdOrder(req.body.order_id, req.body.sender_id)
			.then((result) => {
				console.log(result);

				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	updateOrderRestaurant: function (req, res) {
		order_model
			.updateOrderRestaurant(req.body.order_id, req.body.value)
			.then((result) => {
				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	updateOrderComplete: function (req, res) {
		order_model
			.updateOrderComplete(req.body.order_id, req.body.value)
			.then((result) => {
				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
	getModelOrder: function () {
		return order_model;
	},
	removeOrderSender: async function (req, res) {
		let error, result, token;
		({ error, result } = await functions.to(
			order_model.getOrderByIdOrderAndDataUser(req.params.order_id)
		));
		if (error) res.status(500).json({ message: "not found user" });
		token = result.firebase_token;
		order_model
			.removeOrderById(req.params.order_id)
			.then((result) => {
				push_notification
					.sendmessage(
						[token],
						{ message: "send message only" },
						"ร้านอาหารไม่รับออเดอร์ที่สั่ง",
						"ออเดอรที่" + req.params.order_id
					)
					.then((result) => {
						console.log("push notification restaurant not accept success");
					})
					.catch((err) => {
						console.log("push notification restaurant not accept fail");
					});
				res.status(200).json({ message: result });
			})
			.catch((err) => {
				res.status(404).json({ message: err });
			});
	},
};
